package ar.org.osam.cloud.gateway.configuration;

import ar.org.osam.services.auth.rest.clients.AuthenticationRestClient;
import ar.org.osam.services.auth.security.OsmJwtConfigurer;
import ar.org.osam.services.auth.security.OsmJwtTokenResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /*
     * Services
     */

    @Autowired
    private AuthenticationRestClient authenticationRestClient;
    @Autowired
    private OsmJwtTokenResolver jwtTokenResolver;

    /*
     * Methods
     */

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic()
                    .disable()
                .csrf()
                    .disable() //todo Should enable this.
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .anonymous()
                    .and()
                .authorizeRequests()
                    .antMatchers("/osmauthenticationrestservice/api/authentication/**"
                            ,"/api/resources/auth/accounts/register"
                            ,"/actuator/**"
                            ,"/owl/whatDidTheOwlSay")
                    .permitAll()
                    .and()
                .authorizeRequests()
                    .anyRequest()
                    .authenticated()
                    .and()
                .apply(new OsmJwtConfigurer(authenticationRestClient,jwtTokenResolver));
    }

    /*
     * Beans
     */

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
