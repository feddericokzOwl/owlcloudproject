package ar.org.osam.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import owl.feddericokz.rest.controllers.owl.configuration.EnableOwlWatch;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
@EnableOwlWatch
public class GatewayServerContextConfiguration {

	public static void main(String[] args) {
		SpringApplication.run(GatewayServerContextConfiguration.class, args);
	}

}
