package owl.feddericokz.cloud.servers.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import owl.feddericokz.rest.controllers.owl.configuration.EnableOwlWatch;

@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
@EnableOwlWatch
public class ConfigurationServerContextConfiguration {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationServerContextConfiguration.class, args);
	}

}
