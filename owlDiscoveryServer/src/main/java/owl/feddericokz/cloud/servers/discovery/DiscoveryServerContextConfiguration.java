package owl.feddericokz.cloud.servers.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import owl.feddericokz.rest.controllers.owl.configuration.EnableOwlWatch;

@SpringBootApplication
@EnableEurekaServer
@EnableOwlWatch
public class DiscoveryServerContextConfiguration {

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryServerContextConfiguration.class, args);
	}

}
